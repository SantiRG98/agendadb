package com.example.agendadb;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import database.AgendaDbHelper;

public class MainActivity extends AppCompatActivity {

    private TextView lblRespuesta;
    private Button btnProbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblRespuesta = (TextView)findViewById(R.id.lblRespuesta);
        btnProbar = (Button)findViewById(R.id.btnProbar);

        btnProbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dataBaseName = "";

                SQLiteDatabase db;

                AgendaDbHelper helper = new AgendaDbHelper(MainActivity.this);
                db = helper.getWritableDatabase();
                helper.onUpgrade(db, 1,2);
                dataBaseName = helper.getDatabaseName();

                lblRespuesta.setText(dataBaseName);

            }
        });

    }
}
