package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AgendaDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = "TEXT";
    private static final String INTEGER = "INTEGER";
    private static final String COMMA = " ,";
    private static final String SQL_CREATE_CONTACTO = " CREATE TABLE "
            + DefinirTabla.Contacto.TABLE_NOMBRE
            + " ("
            + DefinirTabla.Contacto._ID + " INTEGER PRIMARY KEY, "
            + DefinirTabla.Contacto.NOMBRE + TEXT_TYPE + COMMA
            + DefinirTabla.Contacto.DOMICILIO + TEXT_TYPE + COMMA
            + DefinirTabla.Contacto.TELEFONO1 + TEXT_TYPE + COMMA
            + DefinirTabla.Contacto.TELEFONO2 + TEXT_TYPE + COMMA
            + DefinirTabla.Contacto.NOTAS + TEXT_TYPE + COMMA
            + DefinirTabla.Contacto.FAVORITO + INTEGER + ")";
    private static final String SQL_DELETE_CONTACTO = "DROP TABLE IF EXISTS " +
            DefinirTabla.Contacto.TABLE_NOMBRE;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "agenda.db";


    public AgendaDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_CONTACTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        sqLiteDatabase.execSQL(SQL_DELETE_CONTACTO);
        onCreate(sqLiteDatabase);
    }

}
